import os
import glob
from mojo import app
from mojo.models import *
from flask import request
from flask_jsonpify import jsonify
from flask_security import Security, login_required, SQLAlchemySessionUserDatastore

# from flask_security import Security, SQLAlchemyUserDatastore, login_required


# Setup Flask-Security
user_datastore = SQLAlchemySessionUserDatastore(db, User, Role)
security = Security(app, user_datastore)
# Setup Flask-Security
# user_datastore = SQLAlchemyUserDatastore(db, User, Role)
# security = Security(app, user_datastore)

# Home
@app.route('/')
def hello_world():
	return 'Welcome to mojo'


# Get all users
@app.route('/mojos', methods = ['GET'])
def get_users():
	users=User.query.all()
	app.logger.debug(users)
	return jsonify({'users' : UserSchema().dump(users, many=True).data}), 200

# Single user
@app.route('/mojos/<int:id>', methods = ['GET', 'DELETE'])
def get_delete_user(id):
	user = User.query.filter_by(id=id).first()
	if user:
		if request.method=='GET':
			return jsonify({'user': UserSchema().dump(user).data}), 200
		if request.method=='DELETE':
			db.session.delete(user)
			db.session.commit()
			return jsonify({'message' : 'User deleted successfully'}), 200
	else:
		return jsonify({'message' : 'User not found with id:'+str(id)}), 404
@app.route('/mojos/<int:id>', methods = ['PATCH'])
def patch_user(id):
	modified = False
	user = User.query.filter_by(id=id).first()
	if user:
		for r in request.json['request']:
			if r['path'] in UserSchema().fields: # Checking whether the attribute exists
				if r['path'] not in UserSchema().dump_only: # Checking whether the attribute is allowed to be modified
					if r['op']=='replace':
						modified = True
						setattr(user, r['path'], r['value'])
				else:
					return jsonify({'message' : 'Invalid request. Not allowed to modify.'}), 400
			else:
				return jsonify({'message' : 'Invalid request. No such attribute.'}), 400
		if modified:
			db.session.add(user)
			db.session.commit()
			return jsonify({'message' : 'User modified successfully',\
							'news' : UserSchema().dump(user).data}), 201
		else:
			return jsonify({'message' : 'Invalid request. Nothing to modify.'}), 400
	else:
		return jsonify({'message' : 'User not found with id:'+str(id)}), 404


# Add a new user
@app.route('/mojos', methods = ['POST'])
def put_user():
	# Parent exist check
	if 'parent_id' in request.json:
		if not User.query.filter_by(id=request.json['parent_id']).first():
			return jsonify({'message' : 'Mojo creation failed, no such parent.'}), 500
	try:
		mojo = UserSchema().load(request.json).data
		db.session.add(mojo)
		db.session.commit()
	except Exception as e:
		# app.logger.debug(e)
		return jsonify({'message' : 'Mojo Creation Failed'}), 500
	else:
		return jsonify({'message' : 'Mojo Created successfully',\
		'mojo' : UserSchema().dump(mojo).data}), 201

# Add a following relationship
@app.route('/mojos/<int:followee_id>/following/<int:follower_id>', methods=['GET','POST', 'DELETE'])
def manage_following(followee_id, follower_id):
	if followee_id==follower_id:
		return jsonify({'message' : "Bad request. Follower and follwee id are same."}), 400
	followee=User.query.filter_by(id=followee_id).first()
	if not followee:
		return jsonify({'message' : 'Follow Relation Adding failed, there is no such followee'}), 404
	follower=User.query.filter_by(id=follower_id).first()
	if follower:
		if request.method == 'GET':
			if follower in followee.following:
				return jsonify({'value' : 'True',\
					'message' : 'Mojo %s follows mojo %s' %(followee_id, follower_id)})
			else:
				return jsonify({'value' : 'False',\
					'message' : 'Mojo %s doesn\'t follow mojo %s' %(followee_id, follower_id)})
		if request.method == 'POST':
			followee.following.append(follower)
			db.session.add(followee)
			db.session.commit()
			return jsonify({'message' : 'Follow relationship added.'}), 201
		if request.method == 'DELETE':
			followee.following.remove(follower)
			db.session.add(followee)
			db.session.commit()
			return jsonify({'message' : 'Follow relationship removed'}), 200
	else:
		return jsonify({'message' : 'Follow Relation Adding failed, there is no such follower'}), 404

# Get followers list of a user
@app.route('/mojos/<int:mojo_id>/following', methods=['GET'])
def get_following_list(mojo_id):
	mojo=User.query.filter_by(id=mojo_id).first()
	if not mojo :
		return jsonify({'message' : 'No such mojo'}), 404
	return jsonify({'following': UserSchema().dump(mojo.following, many=True).data})

# Get followee list of a user
@app.route('/mojos/<int:mojo_id>/followers', methods=['GET'])
def get_followers_list(mojo_id):
	mojo=User.query.filter_by(id=mojo_id).first()
	if not mojo :
		return jsonify({'message' : 'No such mojo'}), 404
	return jsonify({'followers': UserSchema().dump(mojo.followers, many=True).data})

	

'''
# Activate/Deactivate a user
# Eg: /mojos/1/activate/1 --> activates a user
# Eg: /mojos/1/activate/0 --> deactivates a user
@app.route('/mojos/<int:id>/activate/', methods = ['PUT'])
def activate_user(id):
	act=request.json['activate_flag']
	msg='User Deativated'
	user=User.query.filter_by(id=id).first()
	if not user:
		return jsonify({'message': 'No Such User'}), 500
	try:
		user.active=False
		if act:
			user.active=True
			msg='User Activated'
		db.session.add(user)
		db.session.commit()
	except Exception as e:
		return jsonify({'message': 'User Activation Failed'}), 500
	return jsonify({'message': msg}), 201


# Get Roles
@app.route('/roles/', methods=['GET'])
def get_roles():
	roles=Role.query.all()
	return jsonify({'roles' : RoleSchema().dump(roles, many=True).data}), 200

# Get a single Role
@app.route('/roles/<int:id>/', methods=['GET'])
def get_roles_by_id(id):
	role=Role.query.filter_by(id=id).first()
	if role:
		return jsonify({'role': RoleSchema().dump(role).data}), 200
	else:
		return jsonify({'message' : 'Role Do not Exist with id:'+str(id)}), 500


# Add a new role
@app.route('/roles/', methods = ['POST'])
def add_role():
	try:
		db.session.add(RoleSchema().load(request.json).data)
		db.session.commit()
	except Exception as e:
		return jsonify({'message' : 'Role Creation Failed'}), 500
	else:
		return jsonify({'message' :'Role Created successfully'}), 201

# Add a new role to an user
@app.route('/mojos/<int:id>/roles/', methods=['PUT'])
def add_user_role(id):
	user=User.query.filter_by(id=id).first()
	if not user:
		return jsonify({'message' : 'User Role Adding failed, there is no such user'}), 500
	for r in request.json['roles']:
		role=Role.query.filter_by(id=r).first()
		if role:
			user.roles.append(role)
	db.session.add(user)
	db.session.commit()
	return jsonify({'message' : 'User Role Added Successfully'}), 200

'''
# Get all news


# Images
def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1].lower() in app.config['IMG_ALLOWED_EXTENSIONS']

def save_image(item_id, img_type, img_dir):
	image_filename=""
	image=request.files['file']
	# image_pos=str(img_id)
	image_ext=os.path.splitext(image.filename)[1]
	if image.filename=='':
		return False
	if allowed_file(image.filename):
		image_filename=str(item_id)+"_"+img_type+image_ext
		image.save(os.path.join(app.root_path, img_dir, image_filename))
		return os.path.join(img_dir, image_filename)
	else:
		return False

@app.route('/mojos/<int:mojo_id>/images/<string:img_type>', methods = ['GET','DELETE','POST','PATCH','PUT'])
def handle_mojo_images(mojo_id, img_type):
	mojo = User.query.filter_by(id=mojo_id).first()
	if not mojo:
		return jsonify({'message' : 'Mojo not found'}), 404
	
	try:
		img_loc = getattr(mojo, img_type)
	except Exception as e:
		return jsonify({'message' : 'Invalid request. %s not found in mojo %d' % (img_type, mojo_id)}), 400
		

	if request.method == 'GET':
		if img_loc:
			return jsonify({img_type : img_loc}), 200
		else:
			return jsonify({'message' : '%s not found for mojo %d' % (img_type, mojo_id)}), 404

	if request.method == 'DELETE':
		if img_loc:
			try:
				img_path = os.path.join(app.root_path, img_loc)
				if os.path.isfile(img_path): # If file exists delete it
					os.remove(img_path)
					setattr(mojo, img_type, '')
					db.session.add(mojo)
					db.session.commit()
					return jsonify({'message' : 'Image deleted successfully'}), 200
			except Exception as e:
				return jsonify({'message' : 'Image deletion failed. Internal error'}), 400
		else:
			return jsonify({'message' : 'Image deletion failed. No such item exists.'}), 404
	# POST PATCH PUT
	if not request.files:
		return jsonify({'message' : 'Invalid request. Image file not attached.'}), 400

	if request.method=='POST':		
		if img_loc:
			return jsonify({'message' : 'Invalid request. Image exists. Use DELETE or PATCH/PUT instead.'}), 400
		else:
			img_loc=save_image(mojo_id, img_type, app.config['MOJO_IMG_UPLOAD_FOLDER'])
			if img_loc:
				setattr(mojo, img_type, img_loc)
			else:
				return jsonify({'message' : 'Invalid image file extension.'}), 400
			db.session.add(mojo)
			db.session.commit()
			return jsonify({'message' : 'Mojo image uploaded successfully.',\
							'img_loc' : getattr(mojo, img_type)}), 201

	if request.method == 'PUT' or request.method == 'PATCH':
		if not img_loc:
			return jsonify({'message' : 'Image modification failed. No such item exists.'}), 404
		if save_image(mojo_id, img_type, app.config['MOJO_IMG_UPLOAD_FOLDER']):
			return jsonify({'message' : 'Image replaced successfully.',\
							'img_loc' : getattr(mojo, img_type)}), 201
		else:
			return jsonify({'message' : 'Image saving failed.'}), 400

	# if request.method == 'PUT' or request.method == 'POST':

@app.route('/news/<int:news_id>/images/<string:img_type>', methods = ['GET','DELETE','POST','PATCH','PUT'])
def handle_images(news_id,img_type):
	news=News.query.filter_by(id=news_id).first()
	image=Images.query.filter_by(news_id=news_id, img_type=img_type).first()
	
	if not news:
		return jsonify({'message' : 'Invalid news id'}), 404

	if request.method == 'GET':
		if image:
			return jsonify({'image': ImagesSchema(only=['img_loc']).dump(image).data}), 200
		else:
			return jsonify({'message' : 'Image retrieval failed. No such item exists.'}), 404

	if request.method == 'DELETE':
		if image:
			db.session.delete(image)
			db.session.commit()
			return jsonify({'message' : 'Image deleted successfully'}), 200
		else:
			return jsonify({'message' : 'Image deletion failed. No such item exists.'}), 404

	# POST PATCH PUT section
	if not request.files:
		return jsonify({'message' : 'Invalid request. Image file not attached.'}), 400
		
	if request.method=='POST':		
		if image:
			return jsonify({'message' : 'Invalid request. Image exists. Use DELETE or PATCH/PUT instead.'}), 400
		else:
			image=Images()
			image.news_id=news_id
			image.img_type=img_type
			image.img_loc=save_image(news_id, img_type, app.config['NEWS_IMG_UPLOAD_FOLDER'])
			db.session.add(image)
			db.session.commit()
			return jsonify({'message' : 'Image uploaded successfully.',\
							'img_loc' : image.img_loc}), 201

	if request.method == 'PUT' or request.method == 'PATCH':
		if not image:
			return jsonify({'message' : 'Image modification failed. No such item exists.'}), 404
		if save_image(news_id, img_type, app.config['NEWS_IMG_UPLOAD_FOLDER']):
			news.last_modified=datetime.utcnow()
			db.session.add(news)
			db.session.commit()
			return jsonify({'message' : 'Image replaced successfully.',\
							'img_loc' : image.img_loc}), 201
		else:
			return jsonify({'message' : 'Image saving failed.'}), 400


# NEWS

@app.route('/news', methods=['GET'])
def get_news():
	# Mandatory filters
	if not 'state' in request.args:
		return jsonify({'message':'Invalid request. State not found.'}), 400
	state = request.args['state']
	if (not 'from_date' in request.args) or (not 'to_date' in request.args):
		return jsonify({'message':'Invalid request. from_date or to_date not found.'}), 400
	try:
		from_date,to_date = datetime.strptime(request.args['from_date'],"%d-%m-%Y"),datetime.strptime(request.args['to_date'],"%d-%m-%Y")
	except Exception as e:
		return jsonify({'message':'Invalid date format.'}), 400
	if (not 'page' in request.args) or (not 'per_page' in request.args):
		return jsonify({'message': 'Invalid request. page or per_page not found.'}), 400
	page,per_page = int(request.args['page']),int(request.args['per_page'])
	
	# Sorting
	sort = 'by_time'
	if 'sort' in request.args:
		sort = request.args['sort']
	
	q = db.session.query(News).join(Districts).join(States).filter(db.func.lower(States.name)==db.func.lower(state))
	q = q.filter(News.date_time>=from_date,News.date_time<=to_date)
	# Optional filters
	if 'district' in request.args:
		districts=request.args['district'].split(',')
		q = q.filter(db.func.lower(Districts.name).in_([db.func.lower(d) for d in districts]))
	if 'cat' in request.args:
		cats=request.args['cat'].split(',')
		q = q.join(NewsCat).filter(db.func.lower(NewsCat.name).in_([db.func.lower(c) for c in cats]))

	# Sorting
	if sort == 'by_time':
		q = q.order_by(News.date_time.desc())
	elif sort == 'by_view':
		q = q.order_by(News.views.desc())
	q = q.paginate(page, per_page, False)
	return jsonify({'news': NewsSchema().dump(q.items, many=True).data, 'total_items': q.total}), 200

@app.route('/mojos/<int:mojo_id>/following/news', methods=['GET'])
def get_news_following(mojo_id):
	if (not 'from_date' in request.args) or (not 'to_date' in request.args):
		return jsonify({'message':'Invalid request. from_date or to_date not found.'}), 400
	try:
		from_date,to_date = datetime.strptime(request.args['from_date'],"%d-%m-%Y"),datetime.strptime(request.args['to_date'],"%d-%m-%Y")
	except Exception as e:
		return jsonify({'message':'Invalid date format.'}), 400
	# Sorting
	sort = 'by_time'
	if 'sort' in request.args:
		sort = request.args['sort']
	if (not 'page' in request.args) or (not 'per_page' in request.args):
		return jsonify({'message': 'Invalid request. page or per_page not found.'}), 400
	page,per_page = int(request.args['page']),int(request.args['per_page'])

	mojo_following = User.query.filter_by(id=mojo_id).first().following
	if mojo_following:
		q = db.session.query(News).filter(News.mojo_id.in_([m.id for m in mojo_following]))
		# Sorting
		if sort == 'by_time':
			q = q.order_by(News.date_time.desc())
		elif sort == 'by_view':
			q = q.order_by(News.views.desc())
		q = q.paginate(page, per_page, False)
		return jsonify({'news': NewsSchema().dump(q.items, many=True).data, 'total_items': q.total}), 200
	else:
		return jsonify({'message': 'No matching items found.'}), 404

# Single news by its ID
@app.route('/news/<int:id>', methods = ['GET', 'DELETE'])
def get_delete_news_by_id(id):
	news = News.query.filter_by(id=id).first()
	if news:
		if request.method=='GET':
			return jsonify({'news' : NewsSchema().dump(news).data}), 200
		if request.method=='DELETE':
			db.session.delete(news)
			db.session.commit()
			return jsonify({'message' : 'News deleted successfully.'}), 200
	else:
		return jsonify({'message' : 'No news with id:'+str(id)}), 404

# Modify single news item
@app.route('/news/<int:id>', methods = ['PATCH'])
# patch RFC standard format { "op": "replace", "path": "/email", "value": "new.email@example.org" }
def patch_news_by_id(id):
	modified = False
	news = News.query.filter_by(id=id).first()
	if news:
		for r in request.json['request']:
			if r['path'] in NewsSchema().fields: # Checking whether the attribute exists
				if r['path'] not in NewsSchema().dump_only: # Checking whether the attribute is allowed to be modified
					if r['op'] == 'replace':
						setattr(news, r['path'], r['value'])
						modified = True
					if r['op'] == 'increment':
						setattr(news, r['path'], news.__dict__[r['path']]+1)
						modified = True
					if r['op'] == 'decrement':
						setattr(news, r['path'], news.__dict__[r['path']]-1)
						modified = True
					if r['op'] == 'add':
						if r['path'] == 'reportings':
							# modified flag is not required since we are not modifying the news.
							reportings = NewsReportings.query.filter_by(news_id=news.id).first()
							if not reportings:
								reportings = NewsReportings()
								reportings.news = news
							if r['value'] in reportings.__dict__:
								setattr(reportings, r['value'], reportings.__dict__[r['value']]+1)
								db.session.add(reportings)
								db.session.commit()
								return jsonify({'message' : 'News reported successfully',\
								'reportings' : NewsReportingsSchema().dump(reportings).data}), 201
							else:
								return jsonify({'message' : 'Invalid \'value\' for reporting'}), 400

				else:
					return jsonify({'message' : 'Invalid request. Not allowed to modify.'}), 400
			else:
				return jsonify({'message' : 'Invalid request. No such attribute:'+r['path']}), 400
		if modified:
			db.session.add(news)
			db.session.commit()
			return jsonify({'message' : 'News modified successfully',\
							'news' : NewsSchema().dump(news).data}), 201
		else:
			return jsonify({'message' : 'Invalid request. Nothing to modify.'}), 400
	else:
		return jsonify({'message' : 'No news with id:'+str(id)}), 404


# Get news by mojo ID
@app.route('/mojos/<int:mojo_id>/news', methods = ['GET'])
def get_news_mojo(mojo_id):
	if (not 'from_date' in request.args) or (not 'to_date' in request.args):
		return jsonify({'message':'Invalid request. from_date or to_date not found.'}), 400
	try:
		from_date,to_date = datetime.strptime(request.args['from_date'],"%d-%m-%Y"),datetime.strptime(request.args['to_date'],"%d-%m-%Y")
	except Exception as e:
		return jsonify({'message':'Invalid date format.'}), 400
	# Sorting
	sort = 'by_time'
	if 'sort' in request.args:
		sort = request.args['sort']
	if (not 'page' in request.args) or (not 'per_page' in request.args):
		return jsonify({'message': 'Invalid request. page or per_page not found.'}), 400
	page,per_page = int(request.args['page']),int(request.args['per_page'])

	q = db.session.query(News).filter(News.mojo_id==mojo_id)

	# Sorting
	if sort == 'by_time':
		q = q.order_by(News.date_time.desc())
	elif sort == 'by_view':
		q = q.order_by(News.views.desc())
	q = q.paginate(page, per_page, False)
	return jsonify({'news': NewsSchema().dump(q.items, many=True).data, 'total_items': q.total}), 200



@app.route('/news', methods = ['POST'])
def put_news():
	try:
		news=NewsSchema().load(request.json).data
		db.session.add(news)
		db.session.commit()
	except Exception as e:
		app.logger.debug(e)
		return jsonify({'message' : 'News Creation Failed'}), 500
	else:
		return jsonify({'message' : 'News Created Successfully',\
			'news': NewsSchema().dump(news).data}), 201

# EVENT LISTENERS

# Delete image files from file system upon deletion of an image object
@db.event.listens_for(Images, 'after_delete')
def receive_after_delete(mapper, connection, target):
	try:
		image_path = os.path.join(app.root_path, target.img_loc)
		if os.path.isfile(image_path): # If file exists delete it
			os.remove(image_path)
			return True
	except Exception as e:
		return False

# Update last_modified on news update
@db.event.listens_for(News, 'before_update')
def receive_before_update(mapper, connection, target):
	target.last_modified = datetime.utcnow()
	pass
