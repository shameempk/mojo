from mojo import db
from datetime import datetime
from marshmallow import Schema, fields, post_load
from flask_security import UserMixin, RoleMixin


class RolesUsers(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column('user_id', db.Integer(), db.ForeignKey('user.id'))
	role_id = db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))

class Role(db.Model, RoleMixin):
	id = db.Column(db.Integer(), primary_key=True)
	name = db.Column(db.String(80), unique=True)
	description = db.Column(db.String(255))

class RoleSchema(Schema):
	id = fields.Integer()
	name = fields.Str()
	description = fields.Str()

class Following(db.Model):
	followee_id = db.Column('followee_id', db.Integer(), db.ForeignKey('user.id'), primary_key=True)
	follower_id = db.Column('follower_id', db.Integer(), db.ForeignKey('user.id'), primary_key=True) #Mojo being followed

class FollowingSchema(Schema):
	followee_id = fields.Integer()
	follower_id = fields.Integer()

class User(db.Model, UserMixin):
	id = db.Column(db.Integer(), primary_key=True)
	name = db.Column(db.String(255))
	password = db.Column(db.String(255))
	bio = db.Column(db.String(500))
	avatar_img = db.Column(db.String(100))
	cover_img = db.Column(db.String(100))
	district_id = db.Column(db.Integer(), db.ForeignKey('districts.id'))
	district = db.relationship('Districts')
	last_login_at = db.Column(db.DateTime())
	current_login_at = db.Column(db.DateTime())
	last_login_ip = db.Column(db.String(100))
	current_login_ip = db.Column(db.String(100))
	login_count = db.Column(db.Integer)
	active = db.Column(db.Boolean())
	confirmed_at = db.Column(db.DateTime())
	roles = db.relationship('Role', secondary='roles_users', backref=db.backref('users', lazy='dynamic'))
	parent_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
	parent = db.relationship('User', backref='pen_names', remote_side=[id])
	hidden_pen_name = db.Column(db.Boolean(), default=False)
	following = db.relationship('User', secondary='following', backref='followers', \
		primaryjoin=id==Following.followee_id, secondaryjoin=id==Following.follower_id)


class UserSchema(Schema):
	class Meta:
		additional=('id','name','bio','avatar_img','cover_img','district_id','last_login_at','current_login_at',\
			'last_login_ip','current_login_ip','login_count','active','confirmed_at','hidden_pen_name', 'parent_id')
		load_only=('parent_id', 'district_id')
		dump_only=('id','followers_count', 'following_count', 'news_count', 'last_login_at', 'current_login_at', 'last_login_ip'\
			'current_login_at', 'login_count', 'confirmed_at')
	district = fields.Nested('DistrictsSchema')
	roles = fields.Nested('RoleSchema', many=True)
	parent_id = fields.Integer(load_only=True)
	parent = fields.Nested('UserSchema', exclude=['pen_names'])
	pen_names = fields.Nested('UserSchema', exclude=['parent'], many=True)
	followers_count = fields.Function(lambda obj:len(obj.followers))
	following_count = fields.Function(lambda obj:len(obj.following))
	news_count = fields.Function(lambda obj:len(obj.news))
	
	@post_load
	def create_user(self, data):
		return User(**data)


class News(db.Model):
	id=db.Column(db.Integer, primary_key=True)
	mojo_id=db.Column(db.Integer, db.ForeignKey('user.id'))
	mojo=db.relationship('User', backref="news")
	vote=db.Column(db.Integer, default=0)
	head=db.Column(db.String(100))
	body=db.Column(db.Text)
	date_time=db.Column(db.DateTime, default=datetime.utcnow)
	last_modified=db.Column(db.DateTime)
	lat=db.Column(db.Float)
	long=db.Column(db.Float)
	views=db.Column(db.Integer, default=1)
	share_count=db.Column(db.Integer, default=0)
	district_id=db.Column(db.Integer, db.ForeignKey('districts.id'))
	district=db.relationship("Districts", backref=db.backref('news', lazy='dynamic'))
	news_cat_id=db.Column(db.Integer, db.ForeignKey('news_cat.id'))
	news_cat=db.relationship('NewsCat', backref=db.backref('news', lazy='dynamic'))
	status_id=db.Column(db.Integer, db.ForeignKey('news_status.id'))
	status=db.relationship('NewsStatus', backref=db.backref('news', lazy='dynamic'))
	images = db.relationship('Images', cascade="all, delete-orphan")

class NewsSchema(Schema):
	class Meta:
		additional = ('id','vote','head','body','date_time',\
			'lat','long','district_id','news_cat_id','status_id','views','share_count','mojo_id','last_modified')
		dump_only = ('id', 'last_modified', 'date_time')
		load_only = ('mojo_id', 'district_id', 'news_cat_id', 'status_id')
	mojo=fields.Nested('UserSchema', only=['name', 'cover_img', 'avatar_img', 'id'])
	district=fields.Nested('DistrictsSchema')
	news_cat=fields.Nested('NewsCatSchema')
	status=fields.Nested('NewsStatusSchema')
	images=fields.Nested('ImagesSchema', many=True, exclude=['id', 'news_id'])
	reportings = fields.Nested('NewsReportingsSchema', exclude = ['news_id'])

	@post_load
	def make_news(self, data):
		return News(**data)

class Images(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	news_id = db.Column(db.Integer, db.ForeignKey('news.id'))
	img_type = db.Column(db.String(20))
	img_loc = db.Column(db.String(50))

class ImagesSchema(Schema):
	id = fields.Integer()
	news_id = fields.Integer()
	img_type = fields.Str()
	img_loc = fields.Str()

	@post_load
	def make_images(self, data):
		return Images(**data)

class NewsCat(db.Model):
	id=db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(20), unique=True)
	description=db.Column(db.String(100))

class NewsCatSchema(Schema):
	id=fields.Integer()
	name=fields.Str()
	description=fields.Str()

	@post_load
	def make_news_cat(self, data):
		return NewsCat(**data)
		
class NewsStatus(db.Model):
	id=db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(20), unique=True)
	description=db.Column(db.String(100))

class NewsStatusSchema(Schema):
	id=fields.Integer()
	name=fields.Str()
	description=fields.Str()

	@post_load
	def make_news_status(self, data):
		return NewsStatus(**data)

class States(db.Model):
	id=db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(100))

class StatesSchema(Schema):
	id=fields.Integer()
	name=fields.Str()
	districts=fields.Nested('DistrictsSchema', only=['id', 'name'], many=True)

class Districts(db.Model):
	id=db.Column(db.Integer, primary_key=True)
	name=db.Column(db.String(100))
	state_id=db.Column(db.Integer, db.ForeignKey('states.id'))
	state=db.relationship('States', backref='districts')

class DistrictsSchema(Schema):
	id=fields.Integer()
	name=fields.Str()
	state=fields.Nested('StatesSchema', only=['id', 'name'])

class NewsReportings(db.Model):
	news_id = db.Column(db.Integer, db.ForeignKey('news.id'), primary_key=True)
	news = db.relationship('News', backref=db.backref('reportings', cascade='all, delete-orphan', uselist=False), uselist=False )
	spam = db.Column(db.Integer)
	advertisement = db.Column(db.Integer)
	misleading = db.Column(db.Integer)
	controversial = db.Column(db.Integer)

	def __init__(self):
		self.spam = 0
		self.advertisement = 0
		self.misleading = 0
		self.controversial = 0

class NewsReportingsSchema(Schema):
	class Meta:
		additional = ('news_id', 'spam', 'advertisement', 'misleading', 'controversial')

