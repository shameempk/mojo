Installation

1. Configure Python vitualenv https://virtualenv.pypa.io/en/stable/installation/
2. Git clone inside venv
3. Install requirements using
    `pip install -r requirements.txt`
4. Run using
    `python3 run.py`